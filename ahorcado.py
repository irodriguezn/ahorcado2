import entradaSalida as es
import moduloAhorcado as ma

def partida():
    partidaFinalizada=False
    fallos=0
    letrasProbadas=""
    palabraBuscada=ma.eliminaTildes(es.devuelvePalabra()).lower()
    # print("Palabra buscada:", palabraBuscada)
    largo=len(palabraBuscada)
    palabraGuiones="_"*len(palabraBuscada)
    while True:
        probandoPalabra=False
        print("Letras probadas:",letrasProbadas)
        es.intercalar(palabraGuiones)
        print()
        fin=False
        contador=0
        while not fin:
            letra=input("Introduce una letra: ")
            # Si en lugar de letra prueba palabra:
            if len(letra)>1:
                # Comprobamos si acierta la palabra
                probandoPalabra=True
                if letra==palabraBuscada:
                    es.intercalar(palabraBuscada)
                    print("Has ganado, pedazo de máquina!")
                    print()
                    partidaFinalizada=True
                else:
                    fallos+=1
                    es.muestraFallos(fallos)
                break
            # Si probamos una letra
            else:
                # Si dice letra repetida avisamos para que diga otra
                # Si sigue insistiendo en su tontería 3 veces pierde
                if contador<3:
                    if letra in letrasProbadas:
                        print("Estás faba? esa ya la has dicho!!")
                        contador+=1
                    else:
                        fin=True
                else:
                    fin=True
                    print("Pues ahora por tonto te cuelgo!")
                    fallos=6
                    es.muestraFallos(6)

        if not probandoPalabra:
            lPos=ma.compruebaLetra(palabraBuscada, letra)
            letrasProbadas+=letra
            if lPos!=[]:
                palabraGuiones=ma.sustituyeLetra(palabraGuiones,letra,lPos)
            else:
                fallos+=1
                es.muestraFallos(fallos)

        if palabraGuiones==palabraBuscada:
            es.intercalar(palabraGuiones)
            print("Has ganado, pedazo de máquina!")
            print()
            break
        if fallos>=6 or partidaFinalizada:
            if not partidaFinalizada:
                print("Has perdido, borinot")
                print()
            break

def main():
    finPartida=False
    while not finPartida:
        partida()
        op=input("Desea seguir jugando (S/N)")
        if op.upper()!="S":
            finPartida=True

if __name__=="__main__":
    main()

        
