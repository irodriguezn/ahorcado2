from random import randint

def devuelvePalabra():
    lista=[]
    for linea in open("palabras.txt"):
        linea=linea.strip()
        lista.append(linea)
    # Tengo 4 elementos: lista[0], lista[1], ..., lista[3]
    # len(lista)=4
    pos=randint(0,len(lista)-1)
    return lista[pos]

def intercalar(palabra):
    aux=""
    for c in palabra:
        aux=aux+c+" "
    print(aux)

def muestraFallos(fallos):
    if fallos==1:
        print("----- ")
        print("|   | ")
        print("|   o ")
        print("|     ")
        print("|     ")
        print("|\\    ")
        print("--    ")
    elif fallos==2:
        print("----- ")
        print("|   | ")
        print("|   o ")
        print("|   | ")
        print("|     ")
        print("|\\    ")
        print("--    ")        
    elif fallos==3:
        print("----- ")
        print("|   | ")
        print("|   o ")
        print("|  /| ")
        print("|     ")
        print("|\\    ")
        print("--    ")
    elif fallos==4:
        print("----- ")
        print("|   | ")
        print("|   o ")
        print("|  /|\\")
        print("|     ")
        print("|\\    ")
        print("--    ")
    elif fallos==5:
        print("----- ")
        print("|   | ")
        print("|   o ")
        print("|  /|\\")
        print("|  /  ")
        print("|\\    ")
        print("--    ")
    elif fallos==6:
        print("----- ")
        print("|   | ")
        print("|   o ")
        print("|  /|\\")
        print("|  / \\")
        print("|\\    ")
        print("--    ")                
    
if __name__=="__main__":
    for i in range(1,7):
        muestraFallos(i)
