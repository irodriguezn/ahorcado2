# Elimina las tildes de las vocales
# y deja intacta cualquier otra letra
def eliminaTilde(letra):
    if letra in "áäàâ":
        return "a"
    if letra in "éèëê":
        return "e"
    if letra in "íìïî":
        return "i"
    if letra in "óòöô":
        return "o"
    if letra in "úùüû":
        return "u"
    return letra

def eliminaTildes(palabra):
    aux=""
    for car in palabra:
        aux=aux+eliminaTilde(car)
    return aux
    
# Función que comprueba si la letra está en la palabra y que
# devuelve una lista con las posiciones de esta en caso positivo.

def compruebaLetra(palabra, car):
    i=0 # "pepe" "e"
    lista=[]
    while True:
        pos=palabra.find(car,i) # i=0 pos=1 | i=2 pos=3 | i=4 pos=-1
        if pos!=-1:
            i=pos+1 # i=2 | i=4
            lista.append(pos) #lista[1,3]
        else:
            break
    return lista

def sustituyeLetra(palabraG, letra, lPos): # "____" "a" [1,3]
    lista=list(palabraG) # ["_","_","_","_"]
    for pos in lPos: # pos=1 | pos=3
        lista[pos]=letra #  ["_","a","_","_"] | ["_","a","_","a"]
    aux="".join(lista) # "_a_a"
    return aux
